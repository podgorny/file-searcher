# NodeJS command-line file search utility
 
**Parameters**(order is not strict): 
* --EMAIL-TO (required) email address for sending results
* --DIR (required)	- base lookup directory
* --TYPE (optional)	- [D|F] D - directory, F - file
* --PATTERN (optional)	- regular expression to test file/directory name
* --MIN-SIZE (optional)	- minimum file size [B|K|M|G], should be skipped for directories
* --MAX-SIZE (optional)	- maximum file size [B|K|M|G], should be skipped for directories
<br /> (B - bytes, K - kilobytes, M - megabytes, G = gigabytes)

**Usage examples**: 
* index.js --DIR=/User/Files/ --PATTERN=.js --EMAIL-TO=user.name@mail.com
* index.js --DIR=/User/Files/ --TYPE=D --EMAIL-TO=user.name@mail.com
* index.js --PATTERN=.mkv --TYPE=F --MIN-SIZE=4G --DIR=/User/Files/ --EMAIL-TO=user.name@mail.com

const fs = require('fs');
const path = require('path');
let async = require('async');
const excel = require('exceljs');
const mail = require('nodemailer');

const assign = param => process.argv.map(item => item.startsWith(param) ? item.substring(param.length) : '').reduce((prev, curr) => prev + curr);

const emailTo = assign('--EMAIL-TO=');     // (required) email address for sending results
const dir = assign('--DIR=');			    // (required) base lookup directory
const type = assign('--TYPE=');				// (optional) [D|F] D - directory, F - file
const pattern = assign('--PATTERN='); 		// (optional) regexp to test file/directory name
const minSize = assign('--MIN-SIZE=');		// minimum file size [B|K|M|G], should be skipped for directories
const maxSize = assign('--MAX-SIZE=');		// maximum file size [B|K|M|G], should be skipped for directories

const measurementUnits = {
    'B': 1, // B - bytes
    'K': 1024, // K - kilobytes
    'M': 1048576, // M - megabytes
    'G': 1073741824 // G - gigabytes
};

const convertToBytes = item => item.substring(0, item.length - 1) * measurementUnits[item.substring(item.length - 1)];

const compareSize = (limitSize, sign) => fileSize => limitSize ? fileSize * sign <= limitSize * sign : true;
const filterMin = compareSize(convertToBytes(minSize), -1);
const filterMax = compareSize(convertToBytes(maxSize), 1);

const regexp = new RegExp(`.*${pattern}$`);
const testName = item => pattern ? regexp.test(item) : true;

const getFiles = (dir, callback) => {
    fs.readdir(dir, (err, files) => {
        if (err) return callback(err);
        let paths = [];
        async.eachSeries(files, (fileName, eachCallback) => {
            let filePath = path.join(dir, fileName);
            fs.stat(filePath, (err, stat) => {
                if (err) return eachCallback(err);
                if (stat.isDirectory()) {
                    if (type !== 'F' && testName(filePath)) paths.push(filePath);
                    getFiles(filePath, (err, subDirFiles) => {
                        if (err) return eachCallback(err);
                        paths = paths.concat(subDirFiles);
                        eachCallback(null);
                    });
                } else {
                    if (type !== 'D' && testName(filePath) && filterMax(stat.size) && filterMin(stat.size)) {
                        paths.push(filePath);
                    }
                    eachCallback(null);
                }
            });
        }, err => callback(err, paths));
    });
};

const createTable = (data) => {
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet('Files');

    worksheet.columns = [
        {header: 'File', key: 'path', width: 100}
    ];

    worksheet.getCell('A1').style = {
        font: {bold: true, size: 13, color: {argb: 'FFFFFAF0'}},
        alignment: {vertical: 'middle', horizontal: 'center'},
        fill: {type: 'pattern', pattern: 'solid', fgColor: {argb: 'FFc0c0c0'}}
    };

    data.forEach(item => {
        worksheet.addRow({path: item});
    });

    workbook.xlsx.writeFile('files.xlsx').then(() => {
        console.log("saved");
    });
};
const sendMail = () => {
    let mailOptions = {
        from: 'podgorny.yegor@gmail.com',
        to: `${emailTo}`,
        subject: 'Searched files',
        attachments: [{path: './files.xlsx'}]
    };
    fs.readFile('./smtp.properties', 'utf8', (err, data) => {
        if (err) throw err;
        let transporter = mail.createTransport(JSON.parse(data));
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) throw err;
            else console.log('Email.sent: ' + info.response);
        });
    });
};

getFiles(dir, (err, res) => {
    if (err) throw  err;
    createTable(res);
    sendMail();
});
